import { Route, Routes, useNavigate } from "react-router-dom";
import Dashboard from "./pages/dashboard/Dashboard.page";
import LoginPage from "./pages/AuthPages/Login.page";
import NotFound from "./pages/notfound/NotFound.page";
import Layout from "./components/misc/Layout";
import ProtectedRoute from "./services/ProtectedRoute";
import useBoundStore from "./store/Store";
import "./App.css";
import { useEffect } from "react";
import CallDetails from "./pages/CallsSingle./callDetails";

function App() {
  const navigate = useNavigate();
  const authCheck = useBoundStore((state) => {
    return state.user ? state.user : false;
  });
  useEffect(() => {
    if (authCheck === false) navigate("login");
  
  }, [authCheck]);

  return (
    <Layout>
      <div style={{ textAlign: "center", marginTop: "40px" }}>
        <Routes>
          <Route path="login" element={<LoginPage />} />
          <Route path="/" element={<Dashboard />} />
          <Route path="/calldetails/:id" element={<CallDetails />} />
          <Route path="*" element={<NotFound />} />
        </Routes>
      </div>
    </Layout>
  );
}

export default App;
