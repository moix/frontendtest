import { useEffect } from "react";
import useBoundStore from "../../store/Store";
import jwtDecode from "jwt-decode";
import { setSession, getAccessToken } from "../../services/jwt.service";

const Auth = ({ children }) => {
  const { loginWithToken, tokenLoading, logoutService } = useBoundStore(
    (state) => state
  );

  const handleAuthentication = async () => {
    let access_token = getAccessToken();
    if (!access_token) {
      logoutService();
      return;
    }
    if (!isAuthTokenValid(access_token)) return;
    setSession(access_token);
    loginWithToken();
  };

  const isAuthTokenValid = (access_token) => {
    const decoded = jwtDecode(access_token);
    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
      console.warn("access token expired");
      logoutService();
      return false;
    } else {
      return true;
    }
  };
  useEffect(() => {
    handleAuthentication();
  }, []);

  return <div>{tokenLoading ? <div className="h-[100vh] flex flex-col items-center justify-center">
     <h2 className="text-[30px] text-start">Frontend Test</h2>
     <p className="mt-2">Loading.....</p>
  </div> : children}</div>;
};

export default Auth;
