import { AppBar, Toolbar } from "@mui/material";
import { NavLink } from "react-router-dom";
import useBoundStore from "../../store/Store";

const Navbar = () => {
  const { logoutService, user } = useBoundStore((state) => state);
  const onLogout = () => {
    logoutService();
  };
  return (
    <AppBar position="static" sx={{background:"white"}}>
     <div className="container mx-auto">
     <Toolbar sx={{display:"flex",alignItems:"center",justifyContent:"space-between",padding:{md:"0px",sm:"0px",xs:"0px"},}}>
      <NavLink to="/">
        <h3 style={{ color: "black" }}>LOGO</h3>
      </NavLink>
      <div
        style={{
          display: "flex",
          justifyContent: "flex-end",
          alignItems: "center",
          gridColumnGap: "40px",
        }}
      >
       
        {!!user ? (
          <h4 onClick={onLogout} style={{ cursor: "pointer", color: "blue" }}>
            Logout
          </h4>
        ) : (
          <NavLink to="login">
            <h4 style={{ cursor: "pointer", color: "blue" }}>Login</h4>
          </NavLink>
        )}
      </div>
      </Toolbar>
     </div>
    </AppBar>
  );
};

export default Navbar;
