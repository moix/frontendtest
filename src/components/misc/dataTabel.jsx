import { Button, CircularProgress, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useState } from "react";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import useBoundStore from "../../store/Store";
import ModalCustom from "./drawer";
import TabelStyles, { ButtonStyles } from "./styles/styles";
const DataTabels = () => {
    const navigate = useNavigate();
    const [open,setOpen] = useState(false);
    const [selected,setSelected]= useState({});
    const { getCalls,page,calls,setLoading,loading,setSelectedNotes,putCallArchive,gernalLoading,setGernalLoading } = useBoundStore(
        (state) => state
      );
    const getDuration = (time)=>{
        let date = new Date(null);
        date.setSeconds(time);
        let hhmmssFormat = date;
        if(hhmmssFormat.getHours() !=0){
            return hhmmssFormat.getHours()+" hours"+" "+hhmmssFormat.getMinutes()+" Minutes"+" "+hhmmssFormat.getSeconds()+" Secound";
        }else{
            return hhmmssFormat.getMinutes()+" Minutes"+" "+hhmmssFormat.getSeconds()+" Secound";

        }
    }
    const getSecound = (time)=>{
        let date = new Date(null);
        date.setSeconds(time);
        let hhmmssFormat = date;
        return hhmmssFormat.getSeconds()+" Secound";
        
    }
    useEffect(()=>{
        setLoading(true);
        getCalls();
    },[])
    console.log(calls)
    if(loading){
        return(
            <div className="flex justify-center">
                <CircularProgress/>
            </div>
        )
    }
    return ( 
        <div>
            <TableContainer sx={{borderRadius:"4px"}}>
      <Table sx={{ minWidth: 650 }} aria-label="simple table">
        <TableHead sx={{background:"#e2dfdf"}}>
          <TableRow>
            <TableCell sx={{...TabelStyles.heading}}>Call Types</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Direction</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Duration</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>TO</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Via</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Created At</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Status</TableCell>
            <TableCell sx={{...TabelStyles.heading}}>Action</TableCell>
          </TableRow>
        </TableHead>
        {gernalLoading&& <div className="absolute mx-auto w-[100px] mt-2 left-0 right-0">
                <CircularProgress/>
            </div>}
        {calls.rows.lenght!=0 && calls.rows.map((nod,index)=>{
            return(
                <TableBody   sx={{opacity: gernalLoading?0.4:1,cursor:"pointer","&:hover":{background:"#ffa2003d"}}} key={nod.index}>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows,color: nod.call_type=="missed"?"red":"blue"}}>{nod.call_type}</TableCell>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows}}>{nod.direction}</TableCell>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows}}>
                <div>
                   <p className="text-bold">{getDuration(nod.duration)}</p> 
                   <p className="text-bold text-[blue]">({getSecound(nod.duration)})</p> 
                </div>
            </TableCell>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows}}>{nod.to}</TableCell>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows}}>{nod.via}</TableCell>
            <TableCell onClick={()=>{navigate(`/calldetails/${nod.id}`) }} sx={{...TabelStyles.rows}}>{nod.created_at.slice(0,10)}</TableCell>
            <TableCell sx={{...TabelStyles.rows,}}>
                {nod.is_archived?<div 
                onClick={()=>{
                    setGernalLoading(true)
                    putCallArchive(nod.id)
                }} 
                className="p-1 cursor-pointer bg-[#00ffb129] text-[#0808aa]">
                <p className="text-center">Achived</p>

                </div>:
                <div 
                onClick={()=>{
                    setGernalLoading(true)
                    putCallArchive(nod.id)
                }} 
                className=" cursor-pointer p-1 bg-[#ffa2003d] text-[#6c1b8b]">
                <p className="text-center">Not achived</p>
                </div>
                }
                </TableCell>
            <TableCell  sx={{...TabelStyles.rows}}>
                <Button sx={{...ButtonStyles}} 
                onClick={()=>{
                    setSelectedNotes(nod.notes)
                    if(nod.notes!=null)  setSelected({notes:nod.notes,id:nod.id})
                    setOpen(true)
                }
            }
                >Add Note</Button>
            </TableCell>
        </TableBody>
            )
        })}
      </Table>
    </TableContainer>
    <div className="flex flex-col items-center justify-center mt-[20px]">
        <Pagination 
        onChange={(e,p)=>{
            setGernalLoading(true)
            getCalls(p)
            }} 
            page={page}
            count={Math.ceil(calls.count/10)} color="primary"/>
        <p className="mt-2 text-[12px]">{page} - 10 of {calls.count} results</p>

    </div>
      <ModalCustom open={open} selected={selected} 
      handleClose={()=>{
        setOpen(false)
        setSelected({})
    }}
      />
        </div>
     );
}
 
export default DataTabels;