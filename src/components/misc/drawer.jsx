import { Button, CircularProgress, Dialog, DialogContent, DialogTitle, Modal, Slide, TextField } from "@mui/material";
import { forwardRef } from "react";
import useBoundStore from "../../store/Store";
import { ButtonStyles } from "./styles/styles";

const Transition = forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
  });

const ModalCustom = ({open,handleClose,selected}) => {
    const { addNotes,loadingAddNotes,selectedNotes,setLoadingAddNotes } = useBoundStore(
        (state) => state
      );
    const handelNotes = (e) =>{
        e.preventDefault();
        setLoadingAddNotes(true);
        addNotes(selected.id,e.target.notes.value);
        console.log(e.target.notes.value)
    }
    return ( 
      <Dialog
        open={open}
        TransitionComfirstponent={Transition}
        maxWidth={440}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description">
        <DialogTitle>Notes</DialogTitle>
        <DialogContent>
           {loadingAddNotes&& <div className="absolute mx-auto w-[100px] mt-2 left-0 right-0">
                <CircularProgress/>
            </div>}
            <div className="p-[20px]" style={{opacity:loadingAddNotes?0.4:1}}>
                <ul className="list-disc">

                {selectedNotes && open && selectedNotes.map((item,index)=>{
                    return(
                        <li key={index}>{item.content}</li>
                        )
                    })}
                    </ul>
            </div>
            <form className="mt-4 flex justify-between gap-2 w-[100%]" onSubmit={(e)=>handelNotes(e)}>
                <TextField name="notes" type="text" className="flex-1"/>
                <Button type="submit" sx={{...ButtonStyles}}>Add</Button>
            </form>
        </DialogContent>
      </Dialog>
     );
}
 
export default ModalCustom;