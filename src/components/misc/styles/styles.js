export const TabelStyles = {
    heading:{
        textTransform:"uppercase",
        fontWeight:'bold',
        padding:"9px",
        fontSize:"12px"
    },
    rows:{
        padding:"9px",
        textTransform:"capitalize",
        
    }
}
export const ButtonStyles = {
    fontSize:"10px",
    background:"#4040de",
    color:"white",
    "&:hover":{
        background:"#4040de",
    }
}
export default TabelStyles