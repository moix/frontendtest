import { CircularProgress, Paper } from "@mui/material";
import { useEffect } from "react";
import { useParams } from "react-router-dom";
import useBoundStore from "../../store/Store";

const CallDetails = () => {
    const {id} = useParams();
    const {getCallSingle,callDetails,setGernalLoading,gernalLoading} = useBoundStore((state) => state)
    useEffect(() => {
        setGernalLoading(true);
        getCallSingle(id)
    }, [id])
    const getDuration = (time)=>{
        let date = new Date(null);
        date.setSeconds(time);
        let hhmmssFormat = date;
        if(hhmmssFormat.getHours() !=0){
            return hhmmssFormat.getHours()+" hours"+" "+hhmmssFormat.getMinutes()+" Minutes"+" "+hhmmssFormat.getSeconds()+" Secound";
        }else{
            return hhmmssFormat.getMinutes()+" Minutes"+" "+hhmmssFormat.getSeconds()+" Secound";

        }
    }
    const getSecound = (time)=>{
        let date = new Date(null);
        date.setSeconds(time);
        let hhmmssFormat = date;
        return hhmmssFormat.getSeconds()+" Secound";
        
    }
    if(gernalLoading){
        return(
            <div className="mt-[80px] flex justify-center">
                <CircularProgress/>
            </div>
        )
    }
    return ( 
        <div className="container mx-auto mt-[80px]">
          {callDetails && !gernalLoading &&  <Paper>
                <div className="p-4">
                <h1 className="text-[20px]">Call Details</h1>
                <div className="flex flex-col gap-[20px]">
                    <div className="flex gap-[10px]">
                        <h2>Id: </h2>
                        <p>{callDetails.id}</p>
                    </div>
                    <div className="flex gap-[10px]">
                        <h2>To: </h2>
                        <p>{callDetails.to}</p>
                    </div>
                    <div className="flex gap-[10px]">
                    <h2>via: </h2>
                    <p>{callDetails.via}</p>
                </div>
                    <div className="flex gap-[10px]">
                        <h2>Type: </h2>
                        <p className={callDetails.call_type=="missed"?"text-[red]":"text-[blue]"}>{callDetails.call_type}</p>
                    </div>
                    <div className="flex gap-[10px]">
                        <h2>Duration: </h2>
                        <div>
                        <p className="text-[blue]">{getDuration(callDetails.duration)}</p>
                        <p className="text-[red] text-start">{getSecound(callDetails.duration)}</p>
                        </div>
                    </div>
                    <div className="flex gap-[10px] items-center">
                        <h2>Status: </h2>
                        <p> {callDetails.is_archived?<div 
                    className="p-1 cursor-pointer bg-[#00ffb129] text-[#0808aa]">
                    <p className="text-center">Achived</p>

                    </div>:
                    <div 
                    className=" cursor-pointer p-1 bg-[#ffa2003d] text-[#6c1b8b]">
                    <p className="text-center">Not achived</p>
                    </div>
                    }</p>
                    </div>
                   
                    <div className="flex gap-[10px]">
                        <h2>Created At: </h2>
                        <p>{callDetails?.created_at && callDetails?.created_at.slice(0,10)}</p>
                    </div>
                </div>
                </div>
            </Paper>}
        </div>
     );
}
 
export default CallDetails;