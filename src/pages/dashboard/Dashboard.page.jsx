import reactLogo from "../../assets/react.svg";
import DataTabels from "../../components/misc/dataTabel";
const Dashboard = () => {
  return (
    <div className="mt-[80px]">
    <div className="container mx-auto">
      <div>
        <h2 className="text-[30px] text-start">Frontend Test</h2>
      </div>
      <div className="mt-[40px]">
      <DataTabels/>
      </div>
    </div>
    </div>
  );
};

export default Dashboard;
