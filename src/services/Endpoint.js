const Domain = import.meta.env.VITE_ENDPOINT
  ? import.meta.env.VITE_ENDPOINT
  : "https://frontend-test-api.aircall.io";

export default Domain;
