import Domain from "../services/Endpoint";
import axios from "axios";
import { setSession } from "../services/jwt.service";

const createAuthStore = (set) => ({
  user: null,
  authLoading: false,
  tokenLoading: true,
  setUser: (args) => set({ user: args }),
  logoutService: () => {
    setSession(null);
    set({ user: null, authLoading: false, tokenLoading: false });
  },
  loginService: async (username, password) => {
    set({ authLoading: true });
    try {
      const rsp = await axios.post(`${Domain}/auth/login`, {
        username,
        password,
      });
     
      if (rsp.data?.user && rsp.data?.access_token) {
        setSession(rsp.data?.access_token);
        set({ user: rsp.data?.user, authLoading: false });
      } else {
        set({ authLoading: false, user: null });
      }
    } catch (error) {
      console.log(error);
      set({ authLoading: false });
    }
  },
  loginWithToken: async () => {
    try {
      const rsp = await axios.post(`${Domain}/auth/refresh-token`);
      console.log(rsp)
      if (rsp.data?.user && rsp.data?.access_token) {
        setSession(rsp.data?.access_token);
        set({ user: rsp.data?.user, tokenLoading: false });
      } else {
        set({ tokenLoading: false, user: null });
      }
    } catch (error) {
      console.log(error);
      logoutService();
    }
  },
});
export default createAuthStore;
