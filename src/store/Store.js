import create from "zustand";
import { devtools } from "zustand/middleware";
import AuthStore from "./AuthStore";
import CallsStore from "./callsStore";
const useBoundStore = create()(
  devtools((...a) => ({
    ...AuthStore(...a),
    ...CallsStore(...a)
  }))
);
export default useBoundStore;
