import { data } from "autoprefixer";
import axios from "axios";
import Domain from "../services/Endpoint";

const CallsStore = (set,get) => ({
    loading: false,
    gernalLoading: false,
    loadingAddNotes: false,
    callDetails:{},
    page:1,
    calls:{
        rows:[],
        count:0,
    },
    selectedNotes:{},
    setLoading: (val)=>{
        set({loading: val})
    },
    setGernalLoading: (val)=>{
        set({gernalLoading: val})
    },
    setLoadingAddNotes: (val)=>{
        set({loadingAddNotes: val})
    },
    setSelectedNotes:(val)=>{
        set({selectedNotes: val})
    },
    getCalls: async(p) => {
        if(p){
        set({page: p})

        }
        const pg = get().page;
        console.log()
        try {
            const rsp = await axios.get(`${Domain}/calls?offset=${pg}&limit=10`);
            console.log(rsp);
            if(rsp.data?.nodes){
            set({ calls:{rows:rsp.data.nodes,count:rsp.data.totalCount} });
            set({loading: false})
            set({gernalLoading: false})


            }
          } catch (error) {
            console.log(error);
            set({ loading: false });
            set({gernalLoading: false})

          }
    },
    addNotes: async(id,content)=>{
        try {
            const rsp = await axios.post(`${Domain}/calls/${id}/note`,{
                content
            });
            console.log(rsp);
            if(rsp.data){
            get().setSelectedNotes(rsp.data.notes)
            set({loadingAddNotes: false})

            }
          } catch (error) {
            console.log(error);
            set({ loadingAddNotes: false });
          }   
    },
    putCallArchive: async(id)=>{
        try {
            const rsp = await axios.put(`${Domain}/calls/${id}/archive`);
            console.log(rsp);
            if(rsp.data){
            get().getCalls()
            set({gernalLoading: false})

            }
          } catch (error) {
            console.log(error);
            set({ gernalLoading: false });
          }   
    },
    getCallSingle: async(id)=>{
      try {
          const rsp = await axios.get(`${Domain}/calls/${id}`);
          console.log(rsp);
          if(rsp.data){
           set({ gernalLoading: false });
            set({callDetails:rsp.data})

          }
        } catch (error) {
          console.log(error);
          set({ gernalLoading: false });
        }   
  }
    
  });
  
  export default CallsStore;
  